from itertools import accumulate
from bisect import bisect
from random import randrange
from typing import List, Union, Optional
from unicodedata import name as unicode_name


ADJECTIVES = []

NOUNS = []

with open("./names/adjectives.txt", "r") as f:
    for line in f.readlines():
        if not line.strip():
            continue

        ADJECTIVES.append(line.strip())

with open("./names/nouns.txt", "r") as f:
    for line in f.readlines():
        if not line.strip():
            continue

        NOUNS.append(line.strip())


UNICODE_VERSION = 6

# Sauce: http://www.unicode.org/charts/PDF/U1F300.pdf
EMOJI_RANGES_UNICODE = {
    6: [
        ('\U0001F300', '\U0001F320'),
        ('\U0001F330', '\U0001F335'),
        ('\U0001F337', '\U0001F37C'),
        ('\U0001F380', '\U0001F393'),
        ('\U0001F3A0', '\U0001F3C4'),
        ('\U0001F3C6', '\U0001F3CA'),
        ('\U0001F3E0', '\U0001F3F0'),
        ('\U0001F400', '\U0001F43E'),
        ('\U0001F440', ),
        ('\U0001F442', '\U0001F4F7'),
        ('\U0001F4F9', '\U0001F4FC'),
        ('\U0001F500', '\U0001F53C'),
        ('\U0001F540', '\U0001F543'),
        ('\U0001F550', '\U0001F567'),
        ('\U0001F5FB', '\U0001F5FF')
    ],
    7: [
        ('\U0001F300', '\U0001F32C'),
        ('\U0001F330', '\U0001F37D'),
        ('\U0001F380', '\U0001F3CE'),
        ('\U0001F3D4', '\U0001F3F7'),
        ('\U0001F400', '\U0001F4FE'),
        ('\U0001F500', '\U0001F54A'),
        ('\U0001F550', '\U0001F579'),
        ('\U0001F57B', '\U0001F5A3'),
        ('\U0001F5A5', '\U0001F5FF')
    ],
    8: [
        ('\U0001F300', '\U0001F579'),
        ('\U0001F57B', '\U0001F5A3'),
        ('\U0001F5A5', '\U0001F5FF')
    ]
}

NO_NAME_ERROR = '(No name found for this codepoint)'


BANNED_EMOJIS = (
    # emoji 1.0/unicode 6 (https://emojipedia.org/emoji-1.0)
    '\U0001F911',  # Money-Mouth Face
    '\U0001F635',  # Face with Crossed-Out Eyes
    '\U0001F624',  # Face with Steam From Nose
    '\U0001F480',  # Skull
    '\U00002620\U0000FE0F',  # Skull and Crossbones
    '\U0001F4A9',  # Pile of Poo
    '\U0001F4AB',  # Dizzy
    '\U0001F573\U0000FE0F',  # Hole
    '\U0001F443',  # Nose
    '\U0001F445',  # Tongue
    '\U0001F444',  # Mouth
    '\U0001F46E',  # Police Officer
    '\U0001F482',  # Guard
    '\U0001F575\U0000FE0F',  # Detective
    '\U0001F3C7',  # Horse Racing
    '\U000026F7\U0000FE0F',  # Skier
    '\U0001F3C2',  # Snowboarder
    '\U0001F434',  # Horse Face
    '\U0001F40E',  # Horse
    '\U0001F984',  # Unicorn
    '\U0001F341',  # Maple Leaf
    '\U0001F9C0',  # Cheese Wedge
    '\U0001F356',  # Meat on Bone
    '\U0001F357',  # Poultry Leg
    '\U0001F354',  # Hamburger
    '\U0001F32D',  # Hot Dog
    '\U0001F373',  # Cooking
    '\U0001F363',  # Sushi
    '\U0001F364',  # Fried Shrimp
    '\U0001F36B',  # Chocolate Bar
    '\U0001F377',  # Wine Glass
    '\U0001F378',  # Cocktail Glass
    '\U0001F379',  # Tropical Drink
    '\U0001F37A',  # Beer Mug
    '\U0001F37B',  # Clinking Beer Mugs
    '\U0001F3E5',  # Hospital
    '\U0001F6A8',  # Police Car Light
    '\U00002744\U0000FE0F',  # Snowflake
    '\U0001F3BF',  # Skis
    '\U0001F48E',  # Gem Stone
    '\U0001F507',  # Muted Speaker
    '\U0001F515',  # Bell with Slash
    '\U00002697\U0000FE0F',  # Alembic
    '\U0001F48A',  # Pill
    '\U000026B0\U0000FE0F',  # Coffin
    '\U000026B1\U0000FE0F',  # Funeral Urn
    '\U0000267F',  # Wheelchair Symbol
    '\U0001F6B9',  # Men’s Room
    '\U0001F6BA',  # Women’s Room
    '\U0001F6BB',  # Restroom
    '\U0001F6BC',  # Baby Symbol
    '\U0001F6BE',  # Water Closet
    '\U0001F6C2',  # Passport Control
    '\U0001F6C3',  # Customs
    '\U000026A0\U0000FE0F',  # Warning
    '\U0001F6B8',  # Children Crossing
    '\U000026D4',  # No Entry
    '\U0001F6AB',  # Prohibited
    '\U0001F6AD',  # No Smoking
    '\U0001F51E',  # No One Under Eighteen
    '\U00002622\U0000FE0F',  # Radioactive
    '\U00002623\U0000FE0F',  # Biohazard
    '\U0000271D\U0000FE0F',  # Latin Cross
    '\U00002626\U0000FE0F',  # Orthodox Cross
    '\U0000262A\U0000FE0F',  # Star and Crescent
    '\U0000262F\U0000FE0F',  # Yin Yang
    '\U00002638\U0000FE0F',  # Wheel of Dharma
    '\U00002721\U0000FE0F',  # Star of David
    '\U0001F549\U0000FE0F',  # Om
    '\U0000269B\U0000FE0F',  # Atom Symbol
    '\U0001F54E',  # Menorah
    '\U0001F52F',  # Dotted Six-Pointed Star
    '\U0001F6D0',  # Place of Worship
    '\U00002049\U0000FE0F',  # Exclamation Question Mark
    '\U0000203C\U0000FE0F',  # Double Exclamation Mark
    '\U00002757',  # Red Exclamation Mark
    '\U00002755',  # White Exclamation Mark
    '\U0001F4DB',  # Name Badge
    '\U0000274C',  # Cross Mark
    '\U0001F6A9',  # Triangular Flag
)


def random_emoji(unicode_version: Optional[Union[int, List[int]]] = 6):
    if not unicode_version:
        # all versions
        unicode_version = list(EMOJI_RANGES_UNICODE.keys())
    elif isinstance(unicode_version, int):
        unicode_version = [unicode_version]

    emoji_ranges = []
    for version in unicode_version:
        if version in EMOJI_RANGES_UNICODE:
            emoji_ranges.extend(EMOJI_RANGES_UNICODE[version])

    while True:
        # weighted distribution
        count = [ord(r[-1]) - ord(r[0]) + 1 for r in emoji_ranges]
        weight_distr = list(accumulate(count))

        # get one point in the multiple ranges
        point = randrange(weight_distr[-1])

        # select the correct range
        emoji_range_idx = bisect(weight_distr, point)
        emoji_range = emoji_ranges[emoji_range_idx]

        # calculate the index in the selected range
        point_in_range = point
        if emoji_range_idx != 0:
            point_in_range = point - weight_distr[emoji_range_idx - 1]

        # emoji 😄
        emoji = chr(ord(emoji_range[0]) + point_in_range)
        if emoji not in BANNED_EMOJIS:
            break

    emoji_name = unicode_name(emoji, NO_NAME_ERROR).capitalize()
    emoji_codepoint = "U+{}".format(hex(ord(emoji))[2:].upper())

    return emoji, emoji_codepoint, emoji_name
