Eseguire il bot:
1. installare librerie con 'pip install -r requirements.txt'
2. copiare 'config.example.toml' in 'config.toml' e compilare i campi richiesti
3. avviare lo script con 'python main.py'

Migrazioni dabatase:
è buona norma applicare le migrazioni più recenti al database dopo aver pullato il commit più recente:
'alembic upgrade head'
