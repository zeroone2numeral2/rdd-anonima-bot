import datetime
import json
import logging
import logging.config
import random
from typing import Optional, Tuple

from sqlalchemy import ScalarResult
from sqlalchemy.orm import Session
from telegram import Update, MessageId, Message, InlineKeyboardMarkup, InlineKeyboardButton, Bot, BotCommand, \
    BotCommandScopeAllPrivateChats
from telegram.constants import ParseMode
from telegram.error import TelegramError, BadRequest
from telegram.ext import ApplicationBuilder, Application, ContextTypes, CommandHandler, MessageHandler, filters, \
    CallbackQueryHandler, PicklePersistence, PersistenceInput
from telegram.ext import Defaults

import utilities
from constants import TempDataKey, Seconds
from database.base import get_session, Base, engine, session_scope
from database.models import User
from database.queries import users
from names.data import ADJECTIVES, NOUNS, random_emoji
from config import config

logger = logging.getLogger(__name__)

defaults = Defaults(parse_mode=ParseMode.HTML, disable_web_page_preview=True, quote=True)

Base.metadata.create_all(engine)


@utilities.catch_exception()
@utilities.pass_session()
async def on_start_command(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"/start from {update.effective_user.id}")

    user: User = users.get_or_create(session, update.effective_user.id, create_if_missing=False)
    if user and user.banned and user.banned_shadowban:
        # if user is shadowbanned, simply don't answer. We will answer to users that are just banned
        return

    await update.message.reply_text(f"Ciao e benvenutə, questo bot ti consente di pubblicare in anonimo sul gruppo Supporto e Riduzione del Danno Italia.\n\n"
                                    f"Ai tuoi messaggi verrà assegnato un alias casuale che cambia "
                                    f"ogni {config.settings.alias_timeout} ore di inattività, in modo da non confondere "
                                    f"conversazioni con utenti diversi che avvengono in contemporanea. "
                                    f"Puoi rigenerare il tuo alias con /newalias in ogni momento\n\n"
                                    "Inviami il messaggio da pubblicare in anonimo:", quote=False)


def random_alias(number=False):
    adjective = random.choice(ADJECTIVES).capitalize()
    noun = random.choice(NOUNS).capitalize()
    emoji, _, _ = random_emoji()

    if number:
        num = 0
        while num in (0, 69, 14, 88):
            # banned numbers :)))
            num = random.randrange(10, 100)
    else:
        num = ""

    return f"{emoji} {adjective} {noun} {num}".strip()


def get_alias(context: ContextTypes.DEFAULT_TYPE, force_new_alias=False) -> Tuple[str, bool]:
    if force_new_alias or TempDataKey.ALIAS_DATA not in context.user_data:
        logger.info("generating alias")
        alias = random_alias()
        context.user_data[TempDataKey.ALIAS_DATA] = {
            TempDataKey.ALIAS_STRING: alias,
            TempDataKey.ALIAS_LAST_ACTIVITY: utilities.now()
        }
        return alias, True

    diff_seconds = (utilities.now() - context.user_data[TempDataKey.ALIAS_DATA][TempDataKey.ALIAS_LAST_ACTIVITY]).total_seconds()
    if diff_seconds > (config.settings.alias_timeout * Seconds.HOUR_1):
        logger.info("previous alias expired, generating a new one")
        # we reset the alias data after one day of inactivity
        alias = random_alias()
        context.user_data[TempDataKey.ALIAS_DATA] = {
            TempDataKey.ALIAS_STRING: alias,
            TempDataKey.ALIAS_LAST_ACTIVITY: utilities.now()
        }
        return alias, True

    # update user's last activity
    context.user_data[TempDataKey.ALIAS_DATA][TempDataKey.ALIAS_LAST_ACTIVITY] = utilities.now()
    return context.user_data[TempDataKey.ALIAS_DATA][TempDataKey.ALIAS_STRING], False


async def replicate_message(
        bot: Bot,
        chat_id: int,
        message: Message,
        alias: str,
        message_thread_id: Optional[int],
        reply_to_message_id: Optional[int] = None
) -> Optional[Message]:
    kwargs = dict(
        message_thread_id=message_thread_id,
        reply_to_message_id=reply_to_message_id
    )
    if message.has_media_spoiler:
        kwargs["has_spoiler"] = True

    if message.caption:
        kwargs["caption"] = f"<b>{alias} > </b>{message.caption_html}"
    else:
        kwargs["caption"] = f"<b>{alias} ^</b>"

    if message.text:
        kwargs.pop("caption", None)
        text = f"<b>{alias} > </b>{message.text_html}"
        return await bot.send_message(chat_id, text, **kwargs)
    elif message.photo:
        return await bot.send_photo(chat_id, message.photo[-1].file_id, **kwargs)
    elif message.video:
        return await bot.send_video(chat_id, message.video.file_id, **kwargs)
    elif message.document:
        return await bot.send_document(chat_id, message.document.file_id, **kwargs)
    elif message.voice:
        return await bot.send_voice(chat_id, message.voice.file_id, **kwargs)
    elif message.audio:
        return await bot.send_audio(chat_id, message.audio.file_id, **kwargs)
    elif message.animation:
        return await bot.send_animation(chat_id, message.animation.file_id, **kwargs)
    # no message types that do not show a caption
    elif message.sticker or message.video_note or message.location or message.venue:
        # know media types that do not support captions: ignore if the user cannot be identified through their alias
        return
    else:
        return


@utilities.catch_exception()
@utilities.pass_session()
async def on_newalias_command(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"/newalias from {update.effective_user.id}")

    user: User = users.get_or_create(session, update.effective_user.id, create_if_missing=False)
    if user and user.banned:
        # if user is banned, simply ignore this command
        return

    alias, _ = get_alias(context, force_new_alias=True)

    await update.message.reply_text(
        f"Il tuo nuovo alias è <b>{alias}</b>. Verrà resettato dopo {config.settings.alias_timeout} ore di inattività",
        quote=False
    )


def get_thread_id(context: ContextTypes.DEFAULT_TYPE):
    if TempDataKey.USER_MESSAGES_TOPIC_ID not in context.bot_data:
        # Passing "1" (general topic) as message_thread_id will raise a "Message thread not found" exception
        # To post messages in the general topic, we should pass None (or use reply_to_message_id)
        return config.telegram.topic_id if config.telegram.topic_id and config.telegram.topic_id != 1 else None

    return context.bot_data[TempDataKey.USER_MESSAGES_TOPIC_ID]


@utilities.catch_exception()
@utilities.pass_session()
async def on_user_message(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"new private chat message from {update.effective_user.id}")

    user: User = users.get_or_create(session, update.effective_user.id, create_if_missing=False)
    if user and user.banned:
        logger.info("user is banned")
        if not user.banned_shadowban:
            await update.message.reply_text(f"Non puoi inviare messaggi nel gruppo perchè sei stato bloccato da un admin")
        return

    logger.info(f"sending message to {config.telegram.chat_id}/{config.telegram.topic_id}")

    message_thread_id = get_thread_id(context)
    alias, is_new = get_alias(context)
    if is_new:
        await update.message.reply_html(
            f"Ti è stato assegnato il nickname <b>{alias}</b>. Questo nome verrà utilizzato per distinguere i tuoi "
            f"messaggi inviati anonimamente nel gruppo, per evitare confusione e fraintendimenti.\n\n"
            f"E' casuale, non è riconducibile alla tua identità, e viene "
            f"rigenerato dopo {config.settings.alias_timeout} ore dal tuo ultimo messaggio. "
            f"Puoi cambiare il tuo alias in qualsiasi momento con /newalias",
            quote=False
        )

    sent_message: Message = await replicate_message(
        bot=context.bot,
        chat_id=config.telegram.chat_id,
        message=update.message,
        alias=alias,
        message_thread_id=message_thread_id,
    )
    if not sent_message:
        message_dict = update.message.to_dict()

        # anonimize logged message object
        message_dict.pop('from', None)
        message_dict.pop('chat', None)

        logger.info(f"unsupported message to replicate: {message_dict}")
        await update.message.reply_text(f"<i>questo tipo di messaggio non è supportato e non può essere inoltrato nel gruppo</i>")
        return

    # sent_message_id: MessageId = await update.message.copy(config.telegram.chat_id, message_thread_id=message_thread_id)

    context.bot_data[TempDataKey.MESSAGES_IDS][sent_message.message_id] = {
        TempDataKey.USER_ID: update.effective_user.id,
        TempDataKey.SENT_ON: utilities.now()
    }

    if config.settings.allow_revoke:
        reply_markup = InlineKeyboardMarkup(
            [[InlineKeyboardButton("revoca messaggio", callback_data=f"revoke:{update.effective_user.id}:{sent_message.message_id}")]]
        )
    else:
        reply_markup = None

    message_link = utilities.link_from_message_id(config.telegram.chat_id, message_id=sent_message.message_id, topic_id=message_thread_id)
    text = f"Messaggio inviato anonimamente nel gruppo, puoi accedere alla conversazione <a href=\"{message_link}\">da qui</a>. " \
           f"Poi torna in questa chat per continuare a rispondere"
    await update.message.reply_text(text, reply_markup=reply_markup)


async def get_user_id_from_reply(update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[int]:
    if not update.message.reply_to_message:
        await update.message.reply_text("<i>rispondi ad un messaggio inviato dal bot</i>")
        return

    if update.message.reply_to_message.forum_topic_created:
        # message is a reply, but to the "topic created" service message
        await update.message.reply_text("<i>rispondi ad un messaggio inviato dal bot</i>")
        return

    if update.message.reply_to_message.from_user.id != context.bot.id:
        await update.message.reply_text("<i>rispondi ad un messaggio inviato dal bot</i>")
        return

    reply_to_message_id = update.effective_message.reply_to_message.message_id

    if reply_to_message_id not in context.bot_data[TempDataKey.MESSAGES_IDS]:
        await update.message.reply_text(f"il messaggio <code>{reply_to_message_id}</code> non è associato ad alcun utente :(")
        return

    user_id = context.bot_data[TempDataKey.MESSAGES_IDS][reply_to_message_id][TempDataKey.USER_ID]

    return user_id


@utilities.catch_exception()
@utilities.administrators()
@utilities.pass_session()
async def on_ban_command(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"/blocca from {update.effective_user.id}")

    # first thing to do: try to delete the message
    delete_success = await utilities.delete_safe(update.effective_message)

    reason = utilities.get_argument(["blocca", "shadowblocca"], update.effective_message.text)
    shadowban = update.message.text.lower().startswith("/shadowblocca")

    user_id = await get_user_id_from_reply(update, context)
    if not user_id:
        return

    user: User = users.get_or_create(session, user_id)
    user.ban(reason=reason, banned_by=update.effective_user.full_name, shadowban=shadowban)

    if not delete_success:
        await update.message.reply_text("utente bloccato")


@utilities.catch_exception()
@utilities.administrators()
@utilities.pass_session()
async def on_unban_command(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"/sblocca from {update.effective_user.id}")

    # first thing to do: try to delete the message
    delete_success = await utilities.delete_safe(update.effective_message)

    user_id = await get_user_id_from_reply(update, context)
    if not user_id:
        return

    user: User = users.get_or_create(session, user_id, create_if_missing=False)
    if user:
        session.delete(user)

    if not delete_success:
        await update.message.reply_text("utente sbloccato")


@utilities.catch_exception()
@utilities.administrators()
@utilities.pass_session()
async def on_bloccati_command(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Session) -> None:
    logger.info(f"/bloccati from {update.effective_user.id}")

    banned_users: ScalarResult = users.get_banned(session)

    lines = []
    user: User
    for user in banned_users:
        ban_type = "shadownbannato" if user.banned_shadowban else "bannato"
        lines.append(f"<code>{user.user_id}</code> {ban_type} da {user.banned_by_escaped()}, motivo: {user.banned_reason_pretty()}")

    if not lines:
        await update.message.reply_text("-")
        return

    await update.message.reply_text("\n".join(lines))


@utilities.catch_exception()
@utilities.administrators()
async def on_getid_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.info(f"/getid from {update.effective_user.id}")

    text = f"<code>{update.effective_chat.id}</code>"
    try:
        await update.message.reply_text(text)
    except BadRequest as e:
        if "topic_closed" in e.message.lower():
            await context.bot.send_message(update.effective_user.id, text)
            await utilities.delete_safe(update.message)
        else:
            raise e


@utilities.catch_exception()
@utilities.administrators()
async def on_updateadmins_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.info(f"/updateadmins from {update.effective_user.id}")

    logger.info("fetching admins...")
    admins = await context.bot.get_chat_administrators(update.effective_chat.id)
    admin_ids = [admin.user.id for admin in admins]
    admin_names = [utilities.escape(admin.user.full_name) for admin in admins]

    context.chat_data[TempDataKey.CHAT_ADMINS] = {
        TempDataKey.CHAT_ADMINS_LIST: admin_ids,
        TempDataKey.CHAT_ADMINS_SAVED_ON: utilities.now()
    }

    await update.message.reply_text(f"fatto: {', '.join(admin_names)}")


@utilities.catch_exception()
@utilities.administrators()
async def on_settopic_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.info(f"/settopic from {update.effective_user.id}")

    if not update.message.message_thread_id:
        logger.info("new topic for anonymous messages: general")
        context.bot_data[TempDataKey.USER_MESSAGES_TOPIC_ID] = None
    else:
        logger.info(f"new topic for anonymous messages: {update.message.message_thread_id}")
        context.bot_data[TempDataKey.USER_MESSAGES_TOPIC_ID] = update.message.message_thread_id

    new_topic_id = context.bot_data[TempDataKey.USER_MESSAGES_TOPIC_ID] or 1
    await update.message.reply_text(f"questo topic verrà usato per inviare i messaggi degli utenti (ID: <code>{new_topic_id}</code>)")


@utilities.catch_exception()
async def on_revoke_button(update: Update, context: ContextTypes.DEFAULT_TYPE, session: Optional[Session] = None):
    logger.info(f"revoke button from {update.effective_user.id}")

    if not config.settings.allow_revoke:
        await update.callback_query.answer(
            "Al momento non è consentito revocare i propri messaggi inviati tramite questo bot, mi dispiace",
            show_alert=True,
            cache_time=60
        )
        return

    user_id = int(context.matches[0].group("user_id"))
    message_id = int(context.matches[0].group("message_id"))

    if user_id != update.effective_user.id:
        # protection against custom callback queries
        logger.warning(f"callback data user_id ({user_id}) does not match callback user_id ({update.effective_user.id})")
        return

    # bots cannot delete messages older than 48 hours
    # https://core.telegram.org/bots/api#deletemessage
    now = utilities.now()
    hours_threshold = 48
    timedelta_threshold = datetime.timedelta(hours=hours_threshold)
    if update.effective_message.date < (now - timedelta_threshold):
        await update.callback_query.answer(
            f"Mi dispiace, Telegram non mi permette di eliminare messaggi più vecchi di {hours_threshold} ore",
            cache_time=10
        )
        await update.effective_message.edit_reply_markup(reply_markup=None)  # remove the keyboard
        context.bot_data[TempDataKey.MESSAGE_TO_DELETE_CONFIRM].pop(message_id, None)  # try to pop
        return

    if message_id not in context.bot_data[TempDataKey.MESSAGE_TO_DELETE_CONFIRM]:
        context.bot_data[TempDataKey.MESSAGE_TO_DELETE_CONFIRM][message_id] = True
        await update.callback_query.answer(
            "Se sei veramente sicuro di voler eliminare il messaggio inviato nel gruppo, conferma riutilizzando questo tasto.\n"
            "Questa azione non può essere annullata",
            show_alert=True
        )
        return

    context.bot_data[TempDataKey.MESSAGE_TO_DELETE_CONFIRM].pop(message_id, None)
    success = await utilities.delete_by_ids_safe(context.bot, config.telegram.chat_id, message_id)
    result = "messaggio eliminato con successo" if success else "impossibile eliminare messaggio :("
    await update.callback_query.answer(result)

    if success:
        now_str = utilities.strf_dt(utilities.now(it_tz=True, dst_check=True), "%d/%m/%Y alle %H:%M")
        new_text = f"{update.effective_message.text_html}\n\n<b><i>hai eliminato questo messaggio dal gruppo il {now_str}</i></b>"
        await update.callback_query.edit_message_text(new_text, reply_markup=None)


async def drop_old_message_ids(context: ContextTypes.DEFAULT_TYPE):
    logger.info("running job to drop expired message ids...")

    now = utilities.now()

    ids_to_pop = []
    for message_id, message_data in context.bot_data[TempDataKey.MESSAGES_IDS]:
        delta_seconds = (now - message_data[TempDataKey.SENT_ON]).total_seconds()
        if delta_seconds > Seconds.DAYS_2:
            ids_to_pop.append(message_id)

    logger.info(f"dropping {len(ids_to_pop)} ids: {ids_to_pop}")
    for id_to_pop in ids_to_pop:
        context.bot_data[TempDataKey.MESSAGES_IDS].pop(id_to_pop, None)


async def post_init(application: Application) -> None:
    commands = [
        BotCommand("start", "messaggio di benvenuto"),
        BotCommand("newalias", "genera un nuovo alias")
    ]

    await application.bot.set_my_commands(
        commands,
        scope=BotCommandScopeAllPrivateChats()
    )

    if TempDataKey.MESSAGES_IDS not in application.bot_data:
        application.bot_data[TempDataKey.MESSAGES_IDS] = {}

    # We store in this dict the ids of the messages a user wants to delete
    # We ask for confirmation after the first inline button tap,
    # so in this dict we will store the messages for which the user used the button just once
    # We always override the dict on startup, because there's no cleanup job
    application.bot_data[TempDataKey.MESSAGE_TO_DELETE_CONFIRM] = {}


def main():
    with open("logging.json", 'r') as f:
        logging_config = json.load(f)

    logging.config.dictConfig(logging_config)

    persistence = PicklePersistence(
        filepath='data.pickle',
        store_data=PersistenceInput(chat_data=False, user_data=False)  # just store bot_data
    )

    app: Application = ApplicationBuilder() \
        .token(config.telegram.token) \
        .persistence(persistence) \
        .defaults(defaults) \
        .post_init(post_init) \
        .build()

    app.add_handler(CallbackQueryHandler(on_revoke_button, rf"revoke:(?P<user_id>\d+):(?P<message_id>\d+)$"))

    app.add_handler(CommandHandler(["start", "help"], on_start_command, filters=filters.ChatType.PRIVATE))
    app.add_handler(CommandHandler(["newalias"], on_newalias_command, filters=filters.ChatType.PRIVATE))
    app.add_handler(CommandHandler(["settopic", "setopic", "st"], on_settopic_command, filters=filters.Chat(config.telegram.chat_id)))
    app.add_handler(CommandHandler(["blocca", "shadowblocca"], on_ban_command, filters=filters.Chat(config.telegram.chat_id)))
    app.add_handler(CommandHandler(["sblocca"], on_unban_command, filters=filters.Chat(config.telegram.chat_id)))
    app.add_handler(CommandHandler(["bloccati"], on_bloccati_command, filters=filters.Chat(config.telegram.chat_id)))
    app.add_handler(CommandHandler(["updateadmins"], on_updateadmins_command, filters=filters.Chat(config.telegram.chat_id)))

    app.add_handler(CommandHandler(["getid"], on_getid_command))

    app.add_handler(MessageHandler(filters.ChatType.PRIVATE, on_user_message))

    app.job_queue.run_repeating(drop_old_message_ids, interval=Seconds.DAYS_2, first=Seconds.DAYS_2)

    logger.info(f"polling for updates...")
    app.run_polling(
        drop_pending_updates=False,
        allowed_updates=[Update.MESSAGE, Update.EDITED_MESSAGE, Update.CALLBACK_QUERY]
    )


if __name__ == '__main__':
    main()
