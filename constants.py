class TempDataKey:
    MESSAGES_IDS = "messages_ids"
    USER_ID = "user_id"
    SENT_ON = "sent_on"
    DB_INSTANCES = "db_instances"
    CHAT_ADMINS = "chat_admins"
    CHAT_ADMINS_LIST = "chat_admins_list"
    CHAT_ADMINS_SAVED_ON = "chat_admins_saved_on"
    MESSAGE_TO_DELETE_CONFIRM = "message_to_delete_confirm"
    USER_MESSAGES_TOPIC_ID = "user_messages_topic_id"
    ALIAS_DATA = "alias_data"
    ALIAS_STRING = "alias_string"
    ALIAS_LAST_ACTIVITY = "alias_last_activity"


class Seconds:
    SECONDS_10 = 10
    MINUTE_1 = 60
    HOUR_1 = 60 * 60
    HOUR_6 = 60 * 60 * 4
    DAYS_1 = 60 * 60 * 24
    DAYS_2 = 60 * 60 * 24 * 2
    WEEK_1 = 60 * 60 * 24 * 7
