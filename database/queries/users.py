from typing import Optional

from sqlalchemy.orm import Session
from sqlalchemy import true, false, select, null, update

from database.models import User


def get_or_create(session: Session, user_id: int, create_if_missing=True):
    user: User = session.query(User).filter(User.user_id == user_id).one_or_none()

    if not user and create_if_missing:
        user = User(user_id)
        session.add(user)
        session.commit()

    return user


def get_banned(session: Session):
    query = session.query(User).filter(User.banned == true())

    return session.scalars(query)
