import datetime
import json
import logging
from typing import List, Optional, Union, Tuple, Iterable, ClassVar

from sqlalchemy import Column, ForeignKey, Integer, Boolean, String, DateTime, Float, Date
from sqlalchemy.orm import mapped_column

import utilities
from .base import Base, engine

logger = logging.getLogger(__name__)


class User(Base):
    __tablename__ = 'users'

    user_id = mapped_column(Integer, primary_key=True)

    # ban
    banned = Column(Boolean, default=False)
    banned_shadowban = Column(Boolean, default=False)
    banned_reason = Column(String, default=None)
    banned_message_id= Column(Integer, default=None)  # message_id of the chat message
    banned_on = Column(DateTime, default=None)
    banned_by = Column(String, default=None)  # name of the admin
    banned_until = Column(DateTime, default=None)

    def __init__(self, user_id: int):
        self.user_id = user_id

    def ban(self, reason: Optional[str] = None, shadowban=False, banned_by: Optional[str] = None):
        self.banned = True
        self.banned_shadowban = shadowban
        self.banned_reason = reason
        self.banned_by = banned_by
        self.banned_on = utilities.now()

    def unban(self):
        self.banned = False
        self.banned_shadowban = False
        self.banned_reason = None
        self.banned_by = None
        self.banned_on = None

    def set_shadowban(self, value=True):
        """set the user as shadowbanned without overriding description ban metadata"""

        if not self.banned:
            raise ValueError("cannot set shadowban if the user is not banned already")

        self.banned_shadowban = value

    def banned_reason_pretty(self, placeholder: str = "-"):
        return self.banned_reason or placeholder

    def banned_by_escaped(self):
        return utilities.escape(self.banned_by)


