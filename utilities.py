import re
import datetime
import logging.config
import time
from functools import wraps
from typing import Optional, Union, List
from html import escape

import pytz
from pytz.tzinfo import StaticTzInfo, DstTzInfo
from sqlalchemy.orm import Session
from telegram import Update, Message, Bot
from telegram.error import TimedOut, TelegramError, BadRequest
from telegram.ext import CallbackContext

from constants import TempDataKey
from database.base import get_session
from database.models import User
from database.queries import users

logger = logging.getLogger(__name__)


UTC_TIMEZONE = datetime.timezone.utc

ROME_TIMEZONE = pytz.timezone("Europe/Rome")


class DatabaseInstanceKey:
    USER = "db_user"
    SESSION = "session"


def now(it_tz=False, dst_check=False) -> datetime.datetime:
    """Returns the current UTC datetime. If 'it_tz', Rome's timezone will be used"""

    if not it_tz:
        return datetime.datetime.now(tz=UTC_TIMEZONE)
    else:
        now_naive = datetime.datetime.now()
        local_time = ROME_TIMEZONE.localize(now_naive)
        if dst_check and local_time.dst():
            return local_time + local_time.dst()

        return local_time


def strf_dt(dt: datetime.datetime, fmt="%d/%m/%Y %H:%M:%S"):
    return dt.strftime(fmt)


def escape_html(string):
    return escape(str(string))


def catch_exception(silent=False, skip_not_modified_exception=False):
    def real_decorator(func):
        @wraps(func)
        async def wrapped(update: Update, context: CallbackContext, *args, **kwargs):
            try:
                return await func(update, context, *args, **kwargs)
            except TimedOut:
                # what should this return when we are inside a conversation?
                logger.error('Telegram exception: TimedOut')
            except Exception as e:
                logger.error('error while running handler callback: %s', str(e), exc_info=True)

                if not silent:
                    topic_link = "https://t.me/riduzionedeldannoitalia/80170"  # "rdd automatica - la casa dei bot"
                    text = (f"Oops, qualcosa è andato storto: <code>{escape_html(str(e))}</code>\n"
                            f"Se vuoi, segnala l'errore <a href=\"{topic_link}\">nel gruppo</a> oppure ad un membr dello staff")
                    await update.effective_message.reply_html(text)

                # return ConversationHandler.END
                return

        return wrapped

    return real_decorator


def administrators():
    def real_decorator(func):
        @wraps(func)
        async def wrapped(update: Update, context: CallbackContext, *args, **kwargs):
            if TempDataKey.CHAT_ADMINS not in context.chat_data or (now() - context.chat_data[TempDataKey.CHAT_ADMINS][TempDataKey.CHAT_ADMINS_SAVED_ON]).total_seconds() > 60 * 60 * 6:
                chat_id = update.effective_chat.id
                logger.info("no cached admins (or expired): fetching them...")
                context.chat_data[TempDataKey.CHAT_ADMINS] = {
                    TempDataKey.CHAT_ADMINS_LIST: [admin.user.id for admin in await context.bot.get_chat_administrators(chat_id)],
                    TempDataKey.CHAT_ADMINS_SAVED_ON: now()
                }

            if update.effective_user.id not in context.chat_data[TempDataKey.CHAT_ADMINS][TempDataKey.CHAT_ADMINS_LIST]:
                logger.info(f"user {update.effective_user.id} is not an admin")
                return

            return await func(update, context, *args, **kwargs)

        return wrapped

    return real_decorator


def pass_session(
        pass_user=False,
        rollback_on_exception=False,
        commit_on_exception=True,
        pass_down_db_instances=False
):
    # 'rollback_on_exception' should be false by default because we might want to commit
    # what has been added (session.add()) to the session until the exception has been raised anyway.
    # For the same reason, we might want to commit anyway when an exception happens using 'commit_on_exception'

    if all([rollback_on_exception, commit_on_exception]):
        raise ValueError("'rollback_on_exception' and 'commit_on_exception' are mutually exclusive")

    def real_decorator(func):
        @wraps(func)
        async def wrapped(update: Update, context: CallbackContext, *args, **kwargs):
            session: Optional[Session] = None
            user: Optional[User] = None

            if TempDataKey.DB_INSTANCES in context.chat_data:
                logger.debug(f"chat_data contains {TempDataKey.DB_INSTANCES} (session will be recycled)")
                if DatabaseInstanceKey.SESSION not in context.chat_data[TempDataKey.DB_INSTANCES]:
                    raise ValueError("Session object was not passed in chat_data")
                session = context.chat_data[TempDataKey.DB_INSTANCES][DatabaseInstanceKey.SESSION]

                if DatabaseInstanceKey.USER in context.chat_data[TempDataKey.DB_INSTANCES]:
                    # user might be None
                    user = context.chat_data[TempDataKey.DB_INSTANCES][DatabaseInstanceKey.USER]

            # we fetch the session once per message at max, because the decorator is run only if a message passes filters
            # if we are using different handlers groups, the session will be fetched once per  group unless passed down
            if not session:
                logger.debug("fetching a new session")
                session: Session = get_session()

            if not pass_down_db_instances:
                # if not asked to pass them down, we can just pop them
                context.chat_data.pop(TempDataKey.DB_INSTANCES, None)

            if pass_user and update.effective_user:
                if not user:
                    # fetch it only if not passed in chat_data
                    logger.debug("fetching User object")
                    user = users.get_or_create(session, update.effective_user.id)
                kwargs['user'] = user

            # noinspection PyBroadException
            try:
                result = await func(update, context, session=session, *args, **kwargs)
            except Exception as e:
                if rollback_on_exception:
                    logger.warning(f"exception while running an handler callback ({e}): rolling back")
                    session.rollback()

                if commit_on_exception:
                    logger.warning(f"exception while running an handler callback ({e}): committing")
                    session.commit()

                # if an exception happens, we DO NOT pass session/db instances down
                if not pass_down_db_instances:
                    context.chat_data.pop(TempDataKey.DB_INSTANCES, None)

                # raise the exception anyway, so outher decorators can catch it
                raise

            if pass_down_db_instances:
                logger.debug(f"storing db instances in chat_data")
                context.chat_data[TempDataKey.DB_INSTANCES] = {
                    DatabaseInstanceKey.SESSION: session,
                    DatabaseInstanceKey.USER: user
                }

            logger.debug("committing session...")
            session.commit()

            return result

        return wrapped

    return real_decorator


def link_from_message_id(chat_id, message_id, topic_id: Optional[int] = None):
    chat_id = str(chat_id).replace("-100", "")

    if topic_id:
        return f"https://t.me/c/{chat_id}/{topic_id}/{message_id}"
    else:
        return f"https://t.me/c/{chat_id}/{message_id}"


def get_argument(commands: Union[List, str], text: str) -> str:
    if isinstance(commands, str):
        commands = [commands]

    prefixes = "".join("/!")

    for command in commands:
        text = re.sub(rf"^[{prefixes}]{command}\s*", "", text, re.I)

    return text.strip()


async def delete_safe(message: Message) -> bool:
    try:
        await message.delete()
        return True
    except (BadRequest, TelegramError) as e:
        logger.info(f"couldn't delete message {message.message_id}: {e}")
        return False


async def delete_by_ids_safe(bot: Bot, chat_id: int, message_id: int) -> bool:
    try:
        await bot.delete_message(chat_id, message_id)
        return True
    except (BadRequest, TelegramError) as e:
        logger.info(f"couldn't delete message {message_id}: {e}")
        return False
